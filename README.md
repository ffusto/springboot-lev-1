# SpringBoot api #

Creare un'applicazione springboot[[1]](#1)  che soddisfi i seguenti requisiti. Per ogni punto, eseguire un commit sul repository.

0. creare un nuovo branch `cognome_nome`

# 1. endpoint /welcome 

#### 1.1 GET /welcome/{name} ####
esporre un endpoint che, a fronte di una chiamata 

```
 GET /welcome/{name}
```
restituisca una stringa 

``` 
welcome {name}! 
``` 

es:
```
 GET /welcome/temera
```
response
``` 
welcome temera! 
``` 

# 2. endpoint /people

#### 2.1 POST /people ####
esporre un endpoint che riceve in POST un body con le informazioni di una Persona e le salva in memoria.

es. body:

```
{ "name": "nome1", "role": "ruolo1" }
```

#### 2.2 GET /people ####
esporre un endpoint che restituisce la lista delle persone salvate in memoria

```
[
  { "name": "nome1", "role": "ruolo1" },
  { "name": "nome2", "role": "ruolo2" },
  { "name": "nome3", "role": "ruolo2" }
]

```
#### 2.3 GET /people/{name} ####
esporre un endpoint che restituisce la persona specificata dal parametro {name}

es.: GET /people/nome2

```
{ "name": "nome2", "role": "ruolo2" }

```
# 3. Database

#### 3.1 salvataggio su db [[2]](#2) ####
Refactoring del codice scritto in precedenza per persistere su database, anziché in memoria, la lista delle persone

#### 3.2 endpoint di modifica ####
esporre un endpoint che permette di modificare il ruolo della persona.  

POST /people/{name}

```
{ "name": "nome1", "role": "nuovoRuolo" }
```
una volta fatto l'aggiornamento sul database, restituisce l'oggetto aggiornato.

#### 3.3 endpoint di cancellazione ####
esporre un endpoint che permette di cancellare la persona

DELETE /people/{name}

una volta cancellata l'entry dal db, ne restituisce l'ultimo valore.

# 4. Problem solving
#### 4.1 multipli di 3 e multipli di 5 ####
Creare una classe che restituisce i numeri compresi in un intervallo passato come parametro. Nel caso in cui il numero sia multiplo di 3 stampa "_n_ multiplo di tre", se invece è multiplo di 5, scrive "_n_ multiplo di 5".
Per i numeri che sono sia multiplo di 3 che di 5 scriverà "_n_ multiplo di 3 e di 5"

#### 4.2 endpoint che restituisce i valori ####
Creare un endpoint 
GET /multipli 
al quale è possibile passare l'intervallo e che restituisce la lista dei valori stampati dalla classe del punto 4.1


---

[1] per la creazione del progetto, è possibile utilizzare 
[spring initializr](https://start.spring.io/#!type=maven-project&language=java&platformVersion=2.5.4&packaging=jar&jvmVersion=11&groupId=it.temera&artifactId=spring-boot-example&name=spring-boot-example&description=spring%20boot%20example&packageName=it.temera.sbe&dependencies=web,h2) 
	
[2] per semplicità, è possibile usare un database in-memory (tipo H2 Database)
